#!/usr/bin/env -S nix-shell --pure

with import <nixpkgs> {};
with python38Packages;

let
  cpasswords = python38.pkgs.buildPythonApplication rec {
    pname = "cpasswords";
    version = "dev";

    #src = fetchGit {
    #  url = "https://gitlab.crans.org/nounous/cranspasswords.git";
    #  ref = version;
    #};
    src = [ ./. ];

    doCheck = false;

    propagatedBuildInputs = [ polib gpgme paramiko pyperclip dnspython ];

    meta = with lib; {
      description = "This is a centralized passwords manager, using GPG for protection, and allowing shared passwords to groups.";
      homepage    = "https://gitlab.crans.org/nounous/cranspasswords";
      license     = licenses.gpl3;
    };
  };
in mkShell {
  buildInputs = [
    cpasswords
  ];
}
