cPasswords
==========

cPasswords is a group password manager develop by the
`CRANS <https://www.crans.org>`__.

Client installation
-------------------

Add you GPG fingerprint and your SSH key to the cpassword server.
For the Crans, you can do this on the intranet.

Debian-based distribution
~~~~~~~~~~~~~~~~~~~~~~~~~

-  Install some requirements,
   ``apt install git python3 python3-polib python3-paramiko python3-pyperclip python3-gpg python3-pip libgpgme-dev``.

-  Clone the code,
   ``git clone https://gitlab.crans.org/nounous/cranspasswords.git && cd cranspasswords``,

-  Launch ``pip3 install --user .``,

-  Try ``cranspasswords test`` to decrypt a test password.

Nix-based distribution
~~~~~~~~~~~~~~~~~~~~~~

Refer to `shell.nix <shell.nix>`__ to get a Nix environment with cpasswords.

In a Python Virtualenv
~~~~~~~~~~~~~~~~~~~~~~

Make sure you have ``git``, ``gettext`` and Python ``venv`` module.

**Please do not compile ``gpg`` Python module with libgpgme.
It is much cleaner to installer ``python3-gpg``
that will be kept in sync with your ``gpg``.**

-  Clone the code,
   ``git clone https://gitlab.crans.org/nounous/cranspasswords.git && cd cranspasswords``,

-  Create the virtualenv,
   ``python3 -m venv venv --system-site-packages && source venv/bin/activate``

-  Install with ``pip install .``

Server installation
-------------------

-  Clone the code,
   ``git clone git@gitlab.crans.org:nounous/cranspasswords.git``,

-  Launch ``pip3 install --user .``,

-  Add a sudoers rule to enable users from ``sudoers_group`` to launch
   cpasswords,
   ``%sudoers_group ALL=(root) NOPASSWD: /usr/local/bin/cranspasswords-server``

-  Copy and adapt ``docs/serverconfig.example.py`` to
   ``/etc/cranspasswords/serverconfig.py``.

-  Create ``/var/lib/cranspasswords/db/`` with owner root and mod 700
   (root only).

How to
------

Develop
~~~~~~~

With a Python virtualenv,

.. code:: bash

   python3 -m venv venv
   . venv/bin/activate
   pip install -e .

Then you will be able to launch the client with ``cpasswords``
or the server with ``cpasswords-server``.

Update recipients
~~~~~~~~~~~~~~~~~

If you added a recipient in a role, you should use ``--recrypt`` to recrypt
all files.

Change command name
~~~~~~~~~~~~~~~~~~~

If you wish to rename the command from ``cranspasswords`` to something
else, define ``COMMAND_NAME`` environment variable before executing
``pip install`` during the installation of the client or server.

Import cPasswords
~~~~~~~~~~~~~~~~~

To import ``client.py`` from another script, you may need to specify
where is his configuration :

::

   export CRANSPASSWORDS_CLIENT_CONFIG_DIR=/path/to/config/dir/

Build Debian package
~~~~~~~~~~~~~~~~~~~~

You need to install ``debmake dh-python debhelper python3-all``.
Then in the repository root you can run ``dpkg-buildpackage``.
The build files will be in ``../``.

Install completion
------------------

With bash, you can add to ``~/.bashrc`` :

.. code:: bash

   source <this repo path>/docs/bash_completion
   complete -F _cranspasswords_completion <command name>

Troubleshoutings
----------------

To troubleshot, you ``-vv`` option to activate debug.

Sudo asks for a password
~~~~~~~~~~~~~~~~~~~~~~~~

""“sudo: sorry, a password is required to run sudo”""

Check sudoers file on server side.

Cpasswords is not trusting many people
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Please make sure your trustdb is up to date with ``gpg --update-trustdb``.

You need to have physically met and sign at least some people from the server
keyring.
