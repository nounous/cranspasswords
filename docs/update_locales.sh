#!/bin/bash
# Execute docs/update_locales.sh from repo root
xgettext --from-code utf-8 -o messages.pot cpasswords/*.py
msgmerge --update cpasswords/locale/fr/LC_MESSAGES/messages.po messages.pot
rm messages.pot
