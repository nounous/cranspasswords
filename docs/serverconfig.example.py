"""
Configuration Serveur de cranspasswords.

Sont définis ici les utilisateurs et les rôles associés.
Ce fichier est donné à titre d'exemple.
"""

#: Pour override le nom si vous voulez renommer la commande
cmd_name = 'cranspasswords'

#: Chemin vers la commande sendmail
sendmail_cmd = '/usr/lib/sendmail'

#: Répertoire de stockage des mots de passe
STORE = '/var/lib/%s/db/' % (cmd_name,)

#: Ce serveur est-il read-only (on ne peut pas y modifier les mots de passe)
READONLY = False

#: Paramètres mail
SMTP_HOST = "smtp.crans.org"
FROM_MAIL = "%s <root@crans.org>" % cmd_name
TO_MAIL = "root@crans.org"

#: Mapping des utilisateurs et de leurs (mail, fingerprint GPG)
KEYS = {
    'dstan': (u'daniel.stan@crans.org', u'90520CFDE846E7651A1B751FBC9BF8456E1C820B'),
    'legallic': (u'legallic@crans.org', u'4BDD2DC3F10C26B9BC3B0BD93602E1C9A94025B0'),
}

#: Les variables suivantes sont utilisées pour définir le dictionnaire des
#: rôles.
RTC = [
    'dstan'
]

#: Liste des usernames des nounous
NOUNOUS = RTC + [
    'legallic',
]

# Autogen:
#: Liste des usernames des apprentis
APPRENTIS = [
]

#: Liste des usernames des membres du CA
CA = [
]

#: Liste des trésoriers
TRESORERIE = RTC + [
]

#: Les roles utilisés pour savoir qui a le droit le lire/écrire quoi
ROLES = {
    "ca": CA,
    "ca-w": CA,
    "nounous": NOUNOUS,
    "nounous-w": NOUNOUS,
    "apprentis": NOUNOUS + APPRENTIS,
    "apprentis-w": NOUNOUS,
    "tresorerie": TRESORERIE,
    "tresorerie-w": TRESORERIE,
}
