#!/usr/bin/env python3

from setuptools import setup
from os import getenv, path

# Enable the user to install cpasswords
# with another command and config path
command_name = getenv("COMMAND_NAME", "cranspasswords")


def compile_messages():
    """
    Compile gettext translations
    For now, only compile french
    """
    mo_files = []
    locales = ['cpasswords/locale/fr/LC_MESSAGES/messages.po']
    for po_file in locales:
        filename, _ = path.splitext(po_file)
        mo_file = filename + '.mo'
        try:
            from polib import pofile
            po = pofile(po_file)
            po.save_as_mofile(mo_file)
            mo_files.append(mo_file)
        except ImportError:
            print("Polib is not installed, locales will not be compiled.")
            break
    return [('locale', mo_files), ]


setup(
    name="cpasswords",
    version="0.3.0",
    description="Group password manager based on GPG",
    long_description=open('README.rst', encoding='utf-8').read(),
    author="CRANS",
    author_email="roots@crans.org",
    license='GPLv3',
    keywords=['crans', 'passwords', 'gpg', 'ssh', 'group'],
    url="https://gitlab.crans.org/nounous/cranspasswords",
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: POSIX',
        "Programming Language :: Python :: 3",
        'Topic :: Utilities',
    ],
    packages=['cpasswords'],
    data_files=compile_messages(),
    include_package_data=True,
    install_requires=[
        'wheel',
        'paramiko>=2.2',
        'pyperclip>=1.6.4',
        'gpg',
        'dnspython',
    ],
    entry_points={
        "console_scripts": [
            command_name + " = cpasswords.client:main",
            command_name + "-server = cpasswords.server:main",
        ]
    }
)
