import os

if os.environ.get("CRYPTOGRAPHY_OPENSSL_NO_LEGACY") is None:
    os.environ["CRYPTOGRAPHY_OPENSSL_NO_LEGACY"]="1"
