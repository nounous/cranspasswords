"""
Small gettext wrapper

Copyright (C) 2010-2020 Cr@ns <roots@crans.org>
Authors : Daniel Stan <daniel.stan@crans.org>
          Vincent Le Gallic <legallic@crans.org>
          Alexandre Iooss <erdnaxe@crans.org>
SPDX-License-Identifier: GPL-3.0-or-later
"""

import gettext
import platform
try:
    import importlib.resources as importlib_resources
except ImportError:
    # In PY<3.7 fall-back to backported `importlib_resources`.
    import importlib_resources

# Load locale
if platform.uname().system == 'Linux':
    with importlib_resources.path("cpasswords", "locale") as mo_path:
        gettext.bindtextdomain('messages', mo_path)
    gettext.textdomain('messages')
    _ = gettext.gettext
else:
    # Do not support translation under MacOS and Windows
    def _(s):
        return s
